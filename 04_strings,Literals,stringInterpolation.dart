void main() {
  //Literals
  2;
  "Arbj"; //(it is string value)
  4.5; //(double value)
  true; //(Boolen value)

  //various way to define string Literals in dart
  String s1 = 'string';
  String s2 = "Double";
  String s3 = 'It \'s easy';
  String s4 = "it 's easy";
  String s6 = 'hello my self mahmad arbaj ali hawari.'
      ' this is string literals';
  // string interpolation
  String name = "Arbajl";

  print(s6);
  print("The number of characters in string ${name.length}");
}
