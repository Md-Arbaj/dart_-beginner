void main() {
  //finl keyword
  final cityname = 'Naryanghat';
  //final city name = 'Birgunj'
  final String countryname = 'Nepal';

  // constkey word
  const pi = 3.14;
  const double gravity = 9.8;
  {
    //class Circle{
    //  final color.'red';
    //  static const pi = 3.14;

    print("the cityname and countryname is ${cityname} ${countryname}");
  }
}
