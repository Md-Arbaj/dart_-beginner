void main() {
  int a = 2;
  int b = 3;
  int smallNumber;
  if (a < b) {
    smallNumber = a;
  } else {
    smallNumber = b;
  }
  print("$smallNumber is smaller");
}
