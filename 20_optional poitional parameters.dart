void main() {
  printnames("md", "Akhil", "Arbaj");
  printcountriesname("nepal", "Usa", "canada");
}

// requried parameters
void printnames(String name1, String name2, String name3) {
  print("name 1 is $name1");
  print("name 2 is $name2");
  print("name 3 is $name3");
}

// optional poitional parameters
void printcountriesname(String name1, String name2, String name3) {
  print("name 1 is $name1");
  print("name 2 is $name2");
  print("name 3 is $name3");
}
