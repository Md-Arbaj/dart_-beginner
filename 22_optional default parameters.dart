import '21_optional name .dart';

void main() {
  findVolumn(10);
  print("");
  findVolumn(10, breadth: 5, height: 30);
  print("");
  findVolumn(10, height: 30, breadth: 5);
}

void findVoumn(int length, {int breadth = 2, int height = 20}) {
  print("length is $length");
  print("breadth is $breadth");
  print("height is $height");
  print(" Volumn is ${length * breadth * height}");
}
