void main() {
  print("CASE 1");
  // CASE 1: when you know te exception to be thrown,use On clause
  try {
    int result = 12 ~/ 0;
    print("the result is $result");
  } on IntegerDivisionByZeroException {
    print("cannot divide by zero");
  }
  print("");
  print("CASE 2");
  // case 2: when you do not have know the exception use CATCH Clause
  try {
    int result = 12 ~/ 0;
    print("the result is $result");
  } catch (e) {
    print("the exception throw is $e");
  }
  print("");
  print("CASE 3");
  // case 3: using STACK TRACE to know the events occurred before expection was throw
  try {
    int result = 12 ~/ 0;
    print("the result is $result");
  } catch (e, s) {
    print("the exception throw is $e");
    print("STACK TRACE \n $s");
  }
  // case 4 : when there is an exception or not ,finally  CATCH Clause is always
  // excuted
  try {
    int result = 12 ~/ 0;
    print("the result is $result");
  } catch (e) {
    print("the exception throw is $e");
  } finally {
    print("this is FINALLY CLAUSE and is always executed");
  }
}
