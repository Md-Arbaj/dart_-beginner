void main() {
  var student1 = student();
  student1.id = 42;
  student1.name = "Arbaj";
  print("${student1.id}and ${student1.name}");
  student1.study();
  student1.sleep();

  var student2 = student();
  student2.id = 23;
  student2.name = "Akhil";
  print("${student2.id}and ${student2.name}");
  student2.study();
  student2.sleep();
}

// definestate (propertis) and behavior of a student
class student {
  int id = -1; //instance or field variable default values is -1.
  String name = 'akhil'; // instance or field variable default values is null
  void study() {
    print("${this.name}is now studing");
  }

  void sleep() {
    print("${this.name} is now sleeping");
  }
}
