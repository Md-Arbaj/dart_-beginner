void main() {
  var student1 = student();
  student1.id = 23;
  student1.name = "Akhil";
  print("${student1.id} and ${student1.name}");
  student1.study();
  student1.sleep();
}

class student {
  int id = -1;
  String name = 'Akhil';
  student() {
    print("this is my default constructors");
  }
  void study() {
    print("${this.name} is now playing pubg");
  }

  void sleep() {
    print("${this.name} is not sleeping");
  }
}
