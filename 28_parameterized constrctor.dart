void main() {
  var student1 = student();
  student1.id = 23;
  student1.name = "Arbaj";

  print("${student1.id} and ${student1.name}");
  student1.study();
  student1.sleep();
}

class student {
  int id = -1;
  String name = "";

  Student(int id, String name) {
    this.id = id;
    this.name = name;
  }

  void study() {
    print("${this.name} is now playing pubg");
  }

  void sleep() {
    print("${this.name} is not sleeping");
  }
}
